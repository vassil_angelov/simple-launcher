package com.vasko.simplelauncher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class LauncherFragment extends Fragment {

	private ListView mApplicationsListView;
	private ApplicationsAdapter mApplicationsAdapter;
	private View mProgressBarContainer;
	private String mPackage;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_launcher, container,
				false);
		initViews(rootView);
		initAdapter(getActivity());
		setHasOptionsMenu(true);
		mPackage = getActivity().getPackageName();
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.launcher, menu);
		MenuItem searchItem = menu.findItem(R.id.action_search);
		SearchView searchView = (SearchView) MenuItemCompat
				.getActionView(searchItem);
		initSearchView(searchView);
		super.onCreateOptionsMenu(menu, inflater);
	}

	private void initAdapter(Context context) {
		// execute an async task for the creation of the applications adapter
		// because it could considerable amount of time
		new AsyncTask<Context, Void, ApplicationsAdapter>() {
			private Context context;

			@Override
			protected ApplicationsAdapter doInBackground(Context... params) {
				context = params[0];
				return new ApplicationsAdapter(context.getPackageManager());
			}

			@Override
			protected void onPostExecute(ApplicationsAdapter result) {
				super.onPostExecute(result);
				mApplicationsAdapter = result;
				mApplicationsListView.setAdapter(mApplicationsAdapter);
				mProgressBarContainer.setVisibility(View.GONE);
			}
		}.execute(context);
	}

	private void initViews(View rootView) {
		mApplicationsListView = (ListView) rootView
				.findViewById(R.id.applications_list);
		mApplicationsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startApplication(((ApplicationInfo) mApplicationsAdapter
						.getItem(position)));
			}

		});
		mProgressBarContainer = rootView
				.findViewById(R.id.launcher_progress_bar_container);
	}

	private void initSearchView(SearchView searchView) {
		searchView.setQueryHint(getActivity().getString(
				R.string.search_apps_hint));
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String text) {
				mApplicationsAdapter.getFilter().filter(text);
				return true;
			}

			@Override
			public boolean onQueryTextChange(String text) {
				mApplicationsAdapter.getFilter().filter(text);
				return true;
			}
		});
	}

	private void startApplication(ApplicationInfo applicationInfo) {
		String msg = null;
		if (!mPackage.equals(applicationInfo.packageName)) {
			PackageManager manager = getActivity().getPackageManager();
			Intent startIntent = manager
					.getLaunchIntentForPackage(applicationInfo.packageName);
			if (startIntent != null) {
				startActivity(startIntent);
			} else {
				msg = getActivity().getString(R.string.cannot_start);
			}
		} else {
			msg = getActivity().getString(R.string.already_here);
		}
		if (msg != null) {
			Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
		}
	}
}