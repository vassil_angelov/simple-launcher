package com.vasko.simplelauncher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ApplicationsAdapter extends BaseAdapter implements Filterable {

	private class ViewHolder {
		TextView appNameTV;
		ImageView appIconIV;
		int position;
	}

	private List<ApplicationInfo> mAllApplicationsList;
	private List<ApplicationInfo> mCurrentlyDisplayedApplicationsList;
	private PackageManager mPackageManager;

	public ApplicationsAdapter(PackageManager pManager) {
		mPackageManager = pManager;
		mAllApplicationsList = new ArrayList<ApplicationInfo>(
				mPackageManager.getInstalledApplications(0));
		Collections.sort(mAllApplicationsList,
				new Comparator<ApplicationInfo>() {

					@Override
					public int compare(ApplicationInfo lhs, ApplicationInfo rhs) {
						String lhsLabel = mPackageManager.getApplicationLabel(
								lhs).toString();
						String rhsLabel = mPackageManager.getApplicationLabel(
								rhs).toString();
						return lhsLabel.compareTo(rhsLabel);
					}

				});
		mCurrentlyDisplayedApplicationsList = mAllApplicationsList;
	}

	@Override
	public int getCount() {
		return mCurrentlyDisplayedApplicationsList.size();
	}

	@Override
	public Object getItem(int position) {
		return mCurrentlyDisplayedApplicationsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mCurrentlyDisplayedApplicationsList.get(position).uid;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.list_item_app, parent, false);

			ViewHolder holder = new ViewHolder();
			holder.appNameTV = (TextView) convertView
					.findViewById(R.id.app_name);
			holder.appIconIV = (ImageView) convertView
					.findViewById(R.id.app_icon);
			holder.position = position;
			convertView.setTag(holder);
		}
		ViewHolder convertHolder = (ViewHolder) convertView.getTag();
		final ApplicationInfo appInfo = (ApplicationInfo) getItem(position);

		convertHolder.appNameTV.setText(mPackageManager
				.getApplicationLabel(appInfo));
		convertHolder.appIconIV.setImageDrawable(mPackageManager
				.getApplicationIcon(appInfo));

		return convertView;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				ArrayList<ApplicationInfo> filteredApps = new ArrayList<ApplicationInfo>();

				constraint = constraint.toString().toLowerCase();
				boolean hasResult = false;
				for (int i = 0; i < mAllApplicationsList.size(); i++) {
					ApplicationInfo app = mAllApplicationsList.get(i);
					String appName = mPackageManager.getApplicationLabel(app)
							.toString();
					if (appName.toLowerCase().startsWith(constraint.toString())) {
						filteredApps.add(app);
						hasResult = true;
					} else if (hasResult) {
						break;
					}

				}

				results.count = filteredApps.size();
				results.values = filteredApps;

				return results;

			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				mCurrentlyDisplayedApplicationsList = (List<ApplicationInfo>) results.values;
				notifyDataSetChanged();
			}

		};
		return filter;
	}

}
